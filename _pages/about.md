---
layout: about
title: about
permalink: /
description:

profile:
  align: right
  image: prof_pic.jpg
  image-small: prof_pic_384x288.jpg
  image-smaller: prof_pic_330x247.jpg
  address: >
    <p>Australian National University</p>
    <p>Canberra, Australia</p>

news: false  # includes a list of news items
selected_papers: false # includes a list of papers marked as "selected={true}"
social: true  # includes social icons at the bottom of the page
---

I am a PhD student at the Fenner School of Environment and Society at the Australian National University (ANU). My research focuses on developing models of socio-ecological systems. By taking an interdisciplinary approach, I integrate computational and mathematical tools with social and biological knowledge to address environmental challenges. In my PhD, I am developing the concept of resilience as pathway diversity to operationalize resilience and support decision-making in water management. Before joining ANU, I earned a Bachelor’s degree in Physics and a Master’s degree in Complex Systems Modeling from the University of São Paulo, Brazil. I have previously worked with agricultural, forestry, and water systems, utilizing dynamic systems models, agent-based modeling, evolutionary game theory, and social network analysis.

You can find my [Curriculum Vitae]({{ 'vitor_cv.pdf' | prepend: '/assets/pdf/' | relative_url }}) here.
