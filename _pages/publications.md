---
layout: page
permalink: /publications/
title: publications
description:
years: [2024, 2023, 2022, 2021, 2020, 2018]
nav: true
---

<div class="publications">

<h1>journal articles</h1>
{% for y in page.years %}
  <h2 class="year">{{y}}</h2>
  {% bibliography -f papers -q @article[year={{y}}]* %}
{% endfor %}

<h1>theses</h1>
{% bibliography -f papers -q @thesis %}

</div>
