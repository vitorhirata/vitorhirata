# Vitor Hirata Sanches profile

It uses [jekyll](https://jekyllrb.com/) and the [al-folio](https://github.com/alshedivat/al-folio) theme.

## Running the application locally

With [ruby](https://www.ruby-lang.org/en/) version 3.0.0 installed, install the project gems with:
```
bundle install
```

Then build the static site on the `_site` folder running:
```
bundle exec jekyll build
```

And serve the site on `http://localhost:4000` running:
```
bundle exec jekyll serve
```
